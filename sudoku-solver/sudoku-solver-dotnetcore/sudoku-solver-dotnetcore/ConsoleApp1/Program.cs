﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {


            var level = new int?[] {
            null, 3, null, null, 2, 5, 6, null, null,
            null, null, null, null, 8, null, 2, null, null,
            null, null, 2, 1, null, null, null, null, 8,
            3, null, 8, 6, null, null, null, 4, null,
            null, null, 5, null, null, null, 1, null, null,
            null, 6, null, null, null, 7, 3, null, 2,
            9, null, null, null, null, 2, 8, null, null,
            null, null, 1, null, 6, null, null, null, null,
            null, null, 3, 4, 1, null, null, 5, null
            };

            Console.WriteLine("press enter to start solving");
            Console.ReadLine();
            var stopwatch = new Stopwatch();
            stopwatch.Start();

            var solved = solve(level);
            if (solved.Length > 0)
            {
                Console.WriteLine("SOLVED!");
                //                console.log(solved);
            }
            else
            {
                Console.WriteLine("cannot solve");
            }

            stopwatch.Stop();

            Console.WriteLine("Took " + stopwatch.ElapsedMilliseconds + " milliseconds.");

            Console.ReadLine();
        }


        private static bool hasDuplicates(int?[] array)
        {
            array = array.Where(a => a != null).ToArray();
            return array.Length != array.Distinct().Count();
        }

        private static bool isRowValid(int?[] permutation, int rowIndex)
        {
            var row = permutation.Skip(rowIndex * 9).Take(9).ToArray();
            return !hasDuplicates(row);
        }

        private static bool isColumnValid(int?[] permutation, int columnIndex)
        {
            var row = permutation.Where((p, i) => i % 9 == columnIndex).ToArray();
            return !hasDuplicates(row);
        }

        private static bool isRowsValid(int?[] permutation)
        {
            return Enumerable.Range(0, 9).Select(idx => isRowValid(permutation, idx)).All(r => r);
        }

        private static bool isColumnsValid(int?[] permutation)
        {
            return Enumerable.Range(0, 9).Select(idx => isColumnValid(permutation, idx)).All(r => r);
        }

        private static bool isBlocksValid(int?[] permutation)
        {
            return Enumerable.Range(0, 9).Select(idx => isBlockValid(permutation, idx)).All(r => r);
        }

        private static bool isBlockValid(int?[] permutation, int blockIndex)
        {
            var block = permutation.Where((p, i) =>
            {
                var column = i % 9;
                var row = i / 9;
                switch (blockIndex)
                {
                    case 0:
                        return column < 3 && row < 3;
                    case 1:
                        return column < 6 && row < 3 && column > 2;
                    case 2:
                        return column > 5 && row < 3;
                    case 3:
                        return column < 3 && row < 6 && row > 2;
                    case 4:
                        return column > 2 && column < 6 && row < 6 && row > 2;
                    case 5:
                        return column > 5 && row < 6 && row > 2;
                    case 6:
                        return column < 3 && row > 5;
                    case 7:
                        return column > 2 && column < 6 && row > 5;
                    case 8:
                        return column > 5 && row > 5;
                    default:
                        return false;
                }
            });
            return !hasDuplicates(block.ToArray());
        }

        private static bool isPermutationValid(int?[] level, int?[] permutation)
        {
            int i = -1;
            foreach (var l in level)
            {
                i++;
                if (l == null)
                    continue;
                if (permutation.Length <= i)
                    return true;
                if (permutation[i] != l)
                    return false;
            }

            return true;
        }

        private static bool isValid(int?[] level, int?[] permutation)
        {
            var level2 = (int?[])level.Clone();
            for (int i = 0; i < permutation.Length; i++)
            {
                level2[i] = permutation[i];
            }

            return isPermutationValid(level, permutation) &&
                isRowsValid(level2) &&
                isColumnsValid(level2) &&
                isBlocksValid(level2);
        }

        private static int?[] solve(int?[] level)
        {
            var tries = 0;
            var stack = new Stack<int?>();
            stack.Push(1);
            while (stack.Count > 0)
            {
                tries++;
                var canSolve = isValid(level, stack.ToArray().Reverse().ToArray());
                //if (tries % 1000 == 0)
                //                Console.WriteLine("tried to solve " + String.Join(",",stack) + ": " + canSolve + "(" + tries +  " tries)");
                if (canSolve && stack.Count == 81)
                {
                    Console.WriteLine(tries + " permutations evaluated");
                    return stack.ToArray();
                }

                if (canSolve)
                    stack.Push(1);

                if (!canSolve)
                {
                    var previous = stack.Pop();
                    while (previous == 9)
                    {
                        previous = stack.Pop();
                    }
                    stack.Push(previous + 1);
                }
            }

            return null;
        }
    }
}
