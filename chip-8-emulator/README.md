# Chip-8 emulator

This project is a JavaScript emulator of CHIP-8 (https://en.wikipedia.org/wiki/CHIP-8)

### Requirements

1. Node 12 or newer

### Running

1. `npm i`
1. `npm start`

### Tech

- `create-react-app`
- `typescript`

### Hosting

`master` branch build is automatically deployed to https://chip8.panu.dev

### Documentation

- http://devernay.free.fr/hacks/chip8/C8TECH10.HTM
- https://en.wikipedia.org/wiki/CHIP-8#Opcode_table
- https://github.com/ColinEberhardt/wasm-rust-chip8
- https://github.com/Fiaxhs/JShip-8/blob/master/src/main.js
- https://blog.scottlogic.com/2017/12/13/chip8-emulator-webassembly-rust.html
- http://www.multigesture.net/articles/how-to-write-an-emulator-chip-8-interpreter/
- https://fms.komkon.org/EMUL8/HOWTO.html#LABH

### Roms

Roms can be freely distributed. Original pack copied from https://github.com/dmatlack/chip8/tree/master/roms
